# -*- coding: utf-8 -*-
"""1201-week13.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1p0MmMl-t5NcKWWZyvywnen7K2Eo3VfWm
"""

def largest_palindrome_product_of_3_digit_numbers():
  l=[]
  for i in range(100, 999+1):
      for j in range(i, 999+1):
          product = i * j
          if str(product) == str(product)[::-1]:
              l.append(product)
  print(max(l))
largest_palindrome_product_of_3_digit_numbers()